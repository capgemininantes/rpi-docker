import socket, platform, re, sys
from threading import Thread

hostname = platform.node()
port = 8090
files_volume = "/root/input"
template = "/root/stats.html"
FILE_PARAM = "file"

ph=r'^\[\d+\] I\'m (\w+)$'

GLOBAL_TABLES="""
	<table>
		<thead>
			<tr>
				<th>Service Name</th>
				<th>Number of replicas</th>
				<th>Execution time (avg)</th>
				<th>Minimum time</th>
				<th>Maximum time</th>
				<th>Number of tests</th>
			</tr>
		</thead>
		<tbody>
			%s
		</tbody>
	</table>
"""
GLOBAL_TABLES_CONTENT="""
			<tr>
				<td>%s</td>
				<td>%d</td>
				<td>%d ms</td>
				<td>%d ms</td>
				<td>%d ms</td>
				<td>%d</td>
			</tr>
"""
DETAIL_TABLES="""
	<table>
		
		<caption>Details about <b>%s</b></caption>
		<thead>
			<tr>
				<th>Time</th>
				<th>Host</th>
				<th>Result</th>
			</tr>
		</thead>
		<tbody>
			%s
		</tbody>
	</table>
"""
DETAIL_TABLES_CONTENT="""
			<tr>
				<td>%d ms%s</td>
				<td>%s</td>
				<td>%s</td>
			</tr>
"""

if len(sys.argv) > 1:
	template = sys.argv[1]
if len(sys.argv) > 2 and float(sys.argv[2]).is_integer():
	port = int(sys.argv[2])
if len(sys.argv) > 3:
	files_volume = sys.argv[3]

print 'Hostname:',hostname
content = open(template, 'r').read();

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind(('0.0.0.0', port))
s.listen(5)
print 'Listening on port', port

class ConnectionT(Thread):

	def __init__(self, addr, clientsocket):
		Thread.__init__(self)
		self.addr = addr
		self.clientsocket = clientsocket

	def run(self): 
		global content, FILE_PARAM
		
		results = {}
		req = self.clientsocket.recv(4096)
		print "Request:", req
		pattern = r'^GET /?\?(.*) HTTP/\d+\.\d+$'
		file = None
		for l in req.replace('\r', '').split('\n'):
			if re.match(pattern, l):
				params = re.sub(pattern, r'\1', l).split('&')
				for param in params:
					k, v = param.split('=')
					if k == FILE_PARAM:
						file = v
		if file:
			try:
				print 'Open file %s/%s' % (files_volume, file)
				file_content = open('%s/%s' % (files_volume, file), 'r').read()
				
				for line in file_content.split('\n'):
					if len(line)>1:
						id, repl, result, time = line.split('|')
						if id not in results:
							results[id] = {}
							results[id]['r'] = int(repl)
							results[id]['p'] = []
						results[id]['p'].append((result, int(time)))

				ids = [r for r in sorted(results)]
				reps = [results[a]['r'] for a in ids]
				sums = map(lambda y: sum(map(lambda z: z[1], y['p'])), [results[a] for a in ids])
				mins = map(lambda y: min(map(lambda z: z[1], y['p'])), [results[a] for a in ids])
				maxs = map(lambda y: max(map(lambda z: z[1], y['p'])), [results[a] for a in ids])
				tims = map(lambda y: list(map(lambda z: z[1], y['p'])), [results[a] for a in ids])
				msgs = map(lambda y: list(map(lambda z: z[0], y['p'])), [results[a] for a in ids])

				response = ''
				g_msg = ''
				for i in range(len(ids)):
					r = GLOBAL_TABLES_CONTENT % (ids[i], reps[i], int(sums[i]/len(tims[i])), mins[i], maxs[i], len(msgs[i]))
					g_msg += r

				response += GLOBAL_TABLES % g_msg
				for i in range(len(ids)):
					s = ''
					mn = mins[i]
					mx = maxs[i]
					for j in range(len(msgs[i])):
						host = re.sub(ph, r'\1', msgs[i][j])
						s += DETAIL_TABLES_CONTENT % (tims[i][j], (' (min)' if tims[i][j]==mn else ' (max)' if tims[i][j]==mx else ''), host, msgs[i][j])

					response += DETAIL_TABLES % (ids[i], s)

				html_response = re.sub('\$\{tabs\}', response, content)
			except IOError:
				html_response = re.sub('\$\{tabs\}', 'Can not find file \'%s/%s\'' % (files_volume, file), content)
		else:
			html_response = re.sub('\$\{tabs\}', 'Please provide file name as \'file\' GET parameter', content)

		header = {
			'Content-Type': 'text/html; encoding=utf8',
			'Content-Length': len(html_response.encode(encoding="utf-8")),
			'Accept': 'text/html',
			'Connection': 'close',
		}

		response_header = ''.join('%s: %s\r\n' % (k, v) for k, v in header.items())+'\r\n'

		response_proto = 'HTTP/1.1'
		response_code = '200'
		response_status = 'hostname'

		r = '%s %s %s\r\n' % (response_proto, response_code, response_status)
		pk = r.encode(encoding="utf-8")
		pk += response_header.encode(encoding="utf-8")
		pk += (html_response).encode(encoding="utf-8")
		self.clientsocket.send(pk);

		print "Response sent to %s:\n%s\n%s" % (self.addr, pk, "Connection closed")

		self.clientsocket.close()

try:
	while True:
		try:
			c, addr = s.accept()
			print "Got connection from", addr
			client = ConnectionT(addr, c)
			client.start()
		except socket.error:
			print 'Socket error'
except KeyboardInterrupt:
	print 'Program interrupted'
