# Who am I in python for swarm tests

Provides http response with hostname

Usage:
```sh
docker build -t capgemininantes/rpi-whoami ./
docker run --name whoami -d -e WHOAMI_PORT=8080 -e CUSTOM_MESSAGE="[1] I'm \$hostname\$" -p 80:8080 capgemininantes/rpi-whoami
```
