import socket, platform, re, sys
from threading import Thread


# hostname = socket.gethostname()
hostname = platform.node()
port = 8089
custom_message = "I'm $hostname$"
template = './whoami.html'


if len(sys.argv) > 1 and float(sys.argv[1]).is_integer() :
	port = int(sys.argv[1])
if len(sys.argv) > 2:
	custom_message = sys.argv[2]
if len(sys.argv) > 3:
	template = sys.argv[3]

print 'Hostname:', hostname

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind(('0.0.0.0', port))
s.listen(5)
content = open(template, 'r').read();
print 'Listening on port', port

class ConnectionT(Thread):

	def __init__(self, addr, clientsocket):
		Thread.__init__(self)
		self.addr = addr
		self.clientsocket = clientsocket

	def run(self): 
		global content
		
		req = self.clientsocket.recv(4096)
		print "Request:", req

		message_b = re.sub('\$hostname\$', '<b>' + hostname + '</b>', custom_message)
		message = re.sub('\$hostname\$', hostname, custom_message)
		html_response = re.sub('\$\{custom_message\}', message_b, content)

		match = re.match(r'GET / .+User-Agent:\s*([^\r]+).*', re.sub('\r\n', '\\r', req))
		if match:
			agent = match.group(1)
			if 'curl' in agent.lower():
				response = message + "\r\n"
			else:
				response = html_response

		header = {
			'Content-Type': 'text/html; encoding=utf8',
			'Content-Length': len(response.encode(encoding="utf-8")),
			'Accept': 'text/html',
			'Connection': 'close',
		}

		response_header = ''.join('%s: %s\r\n' % (k, v) for k, v in header.items())+'\r\n'

		response_proto = 'HTTP/1.1'
		response_code = '200'
		response_status = 'hostname'

		r = '%s %s %s\r\n' % (response_proto, response_code, response_status)
		pk = r.encode(encoding="utf-8")
		pk += response_header.encode(encoding="utf-8")
		pk += (response).encode(encoding="utf-8")
		self.clientsocket.send(pk);

		print "Response sent to %s:\n%s\n%s" % (self.addr, pk, "Connection closed")

		self.clientsocket.close()

try:
	while True:
		try:
			c, addr = s.accept()
			print "Got connection from", addr
			client = ConnectionT(addr, c)
			client.start()
		except socket.error:
			print 'Socket error'
except KeyboardInterrupt:
	print 'Program interrupted'
